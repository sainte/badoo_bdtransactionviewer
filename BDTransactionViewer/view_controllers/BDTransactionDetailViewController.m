//
//  BDTransactionDetailViewController.m
//  BDTransactionViewer
//
//  Created by Tiago Bencardino on 27/08/16.
//  Copyright © 2016 Badoo. All rights reserved.
//

#import "BDTransactionDetailViewController.h"
#import "BDTransaction.h"
#import "BDTransactionHelper.h"

NSString * const BDTransactionDetailCellReuseIdentifier = @"BDTransactionDetailCell";

@interface BDTransactionDetailViewController ()

@end

@implementation BDTransactionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.transactions.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:BDTransactionDetailCellReuseIdentifier];
    
    BDTransaction *transaction = self.transactions[indexPath.row];
    
    UILabel *transactionLocalizedLabel = [cell viewWithTag:1];
    transactionLocalizedLabel.text = [transaction localizedAmout];
    
    UILabel *transactionsCountLabel = [cell viewWithTag:2];
    transactionsCountLabel.text = [transaction localizedGBPAmount];
    
    return cell;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return [NSString stringWithFormat:@"Total: %@", [[BDTransactionHelper sharedHelper] localizedSumTransactionsInGBP:self.transactions]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
