//
//  BDTransactionDetailViewController.h
//  BDTransactionViewer
//
//  Created by Tiago Bencardino on 27/08/16.
//  Copyright © 2016 Badoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BDTransactionDetailViewController : UIViewController <UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSString *sku;
@property (nonatomic, strong) NSArray  *transactions;

@end
