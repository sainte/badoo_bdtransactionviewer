//
//  ViewController.m
//  BDTransactionViewer
//
//  Created by Tiago Bencardino on 27/08/16.
//  Copyright © 2016 Badoo. All rights reserved.
//

#import "BDTransactionListViewController.h"
#import "BDDataManager.h"
#import "BDTransaction.h"
#import "BDTransactionDetailViewController.h"

NSString * const BDTransactionListCellReuseIdentifier = @"BDTransactionListCell";
NSString * const BDTransactionDetailViewControllerSegue = @"BDTransactionDetailViewController";

@interface BDTransactionListViewController ()

@property (nonatomic, strong) NSDictionary *transactions;
@property (nonatomic, strong) NSArray *skus;

@end

@implementation BDTransactionListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.transactions =  [[BDDataManager sharedManager] groupedSKUTransactions];
    self.skus = self.transactions.allKeys;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.transactions.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:BDTransactionListCellReuseIdentifier];
    
    NSString *sku = self.skus[indexPath.row];
    NSInteger transactionsCount = [self.transactions[sku] count];
    
    UILabel *productNameLabel = [cell viewWithTag:1];
    productNameLabel.text = sku;
    
    UILabel *transactionsCountLabel = [cell viewWithTag:2];
    transactionsCountLabel.text = [NSString stringWithFormat:@"%ld transactions", transactionsCount];
    
    return cell;
}

#pragma mark - navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:BDTransactionDetailViewControllerSegue] && [sender isKindOfClass:[UITableViewCell class]]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        NSString *sku = self.skus[indexPath.row];
        NSArray *transactions = self.transactions[sku];
        
        UIViewController *destinationVC = segue.destinationViewController;
        if ([destinationVC isKindOfClass:[BDTransactionDetailViewController class]]) {
            BDTransactionDetailViewController *detailVC = (BDTransactionDetailViewController*)destinationVC;
            
            detailVC.sku = sku;
            detailVC.transactions = transactions;
        }
    }
}

@end
