//
//  BDTransactionHelper.m
//  BDTransactionViewer
//
//  Created by Tiago Bencardino on 27/08/16.
//  Copyright © 2016 Badoo. All rights reserved.
//

#import "BDTransactionHelper.h"
#import "BDDataManager.h"
#import "BDTransaction.h"
#import "BDRate.h"

@interface BDTransactionHelper()

@property (nonatomic, strong) NSMutableArray *rates;

@end

@implementation BDTransactionHelper

+ (id)sharedHelper {
    
    static BDTransactionHelper * sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [BDTransactionHelper new];
        sharedManager.rates = [[[BDDataManager sharedManager] rates] mutableCopy];
        
    });
    return sharedManager;
}


- (NSDecimalNumber*)convertTransactionToGBP:(BDTransaction*)transaction {
    
    NSString *from = transaction.currency;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"from == %@ AND to == %@", from, @"GBP"];
    
    BDRate *rate = [[self.rates filteredArrayUsingPredicate:predicate] firstObject];
    if (rate) {
        return [self convertTransaction:transaction rate:rate];
    } else {
        return [self attemptCreateRateAndConvertTransation:transaction];
    }
}

- (NSDecimalNumber*)attemptCreateRateAndConvertTransation:(BDTransaction*)transaction {
 
    NSString *from = transaction.currency;
    
    //1st level
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"from == %@", from];
    NSArray *fromSameFromCurrency = [self.rates filteredArrayUsingPredicate:predicate];
    
    for (BDRate *rate1st in fromSameFromCurrency) {
        //2nd level
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"from == %@ AND to == %@", rate1st.to, @"GBP"];
        BDRate *rate2nd = [[self.rates filteredArrayUsingPredicate:predicate2] firstObject];
        if (rate2nd) {
            
            BDRate *newRate = [self composedRateWithFirst:rate1st second:rate2nd];
            [self.rates addObject:newRate];
            return [self convertTransaction:transaction rate:newRate];
        }
    }
    return nil;
}

- (BDRate*)composedRateWithFirst:(BDRate*)rate1st second:(BDRate*)rate2nd {
    
    BDRate *newRate = [BDRate new];
    newRate.from = rate1st.from;
    newRate.to = rate2nd.to;
    
    NSDecimalNumber *first = [NSDecimalNumber decimalNumberWithString:rate1st.rate];
    NSDecimalNumber *second = [NSDecimalNumber decimalNumberWithString:rate2nd.rate];
    NSDecimalNumber *rateValue = [first decimalNumberByMultiplyingBy:second];
    
    newRate.rate = [rateValue description];
    
    return newRate;
}
- (NSDecimalNumber*)convertTransaction:(BDTransaction*)transaction rate:(BDRate*)rate {
    
    NSDecimalNumber *amount = [NSDecimalNumber decimalNumberWithString:transaction.amount];
    NSDecimalNumber *rateValue = [NSDecimalNumber decimalNumberWithString:rate.rate];
    NSDecimalNumber *amountGBP = [amount decimalNumberByMultiplyingBy:rateValue];

    return amountGBP;
}

- (NSString*)localizedSumTransactionsInGBP:(NSArray*)transactions {
    
    NSDecimalNumber *sum = [[NSDecimalNumber alloc] initWithInt:0];
    for (BDTransaction *transaction in transactions) {
        sum = [sum decimalNumberByAdding:[transaction GBPAmount]];
    }
    
    return [NSString stringWithFormat:@"£ %.2f", sum.floatValue];
}

@end
