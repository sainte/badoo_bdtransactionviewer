//
//  BDDataManager.m
//  BDTransactionViewer
//
//  Created by Tiago Bencardino on 27/08/16.
//  Copyright © 2016 Badoo. All rights reserved.
//

#import "BDDataManager.h"
#import "BDTransaction.h"
#import "BDRate.h"

@interface BDDataManager()

@end

@implementation BDDataManager

+ (id)sharedManager {
    
    static BDDataManager * sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [BDDataManager new];
    });
    return sharedManager;
}

#pragma mark - lazy instanciations 

- (NSArray*)transactions {
    
    if (!_transactions) {
        NSArray *transactionsPlist = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"transactions" ofType:@"plist"]];
        
        NSMutableArray *transactions = [NSMutableArray new];
        for (NSDictionary *transactionDict in transactionsPlist) {
            BDTransaction *transaction = [[BDTransaction alloc] initWithDictionary:transactionDict];
            if (transaction) {
                [transactions addObject:transaction];
            }
        }
        _transactions = transactions;
    }
    
    return _transactions;
}

- (NSArray*)rates {
    
    if (!_rates) {
        NSArray *ratesPlist = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"rates" ofType:@"plist"]];
        
        NSMutableArray *rates = [NSMutableArray new];
        for (NSDictionary *rateDict in ratesPlist) {
            BDRate *rate = [[BDRate alloc] initWithDictionary:rateDict];
            if (rate) {
                [rates addObject:rate];
            }
        }
        _rates = rates;
    }
    
    return _rates;
}

- (NSDictionary*)groupedSKUTransactions {
    
    NSArray *uniqueSKUs = [self.transactions valueForKeyPath:@"@distinctUnionOfObjects.sku"];
    NSMutableDictionary *organizedTransactions = [NSMutableDictionary new];
    
    for (NSString *sku in uniqueSKUs) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sku == %@", sku];
        NSArray *filteredTransactions = [self.transactions filteredArrayUsingPredicate:predicate];
        organizedTransactions[sku] = filteredTransactions;
    }
    return organizedTransactions;
}

@end
