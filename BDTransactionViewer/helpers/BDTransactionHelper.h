//
//  BDTransactionHelper.h
//  BDTransactionViewer
//
//  Created by Tiago Bencardino on 27/08/16.
//  Copyright © 2016 Badoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BDTransaction.h"

@interface BDTransactionHelper : NSObject

+ (id)sharedHelper;

- (NSDecimalNumber*)convertTransactionToGBP:(BDTransaction*)transaction;
- (NSString*)localizedSumTransactionsInGBP:(NSArray*)transactions;

@end
