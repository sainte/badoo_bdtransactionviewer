//
//  BDDataManager.h
//  BDTransactionViewer
//
//  Created by Tiago Bencardino on 27/08/16.
//  Copyright © 2016 Badoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BDDataManager : NSObject

@property (nonatomic, strong) NSArray *transactions;
@property (nonatomic, strong) NSArray *rates;

+ (id)sharedManager;
- (NSDictionary*)groupedSKUTransactions;

@end
