//
//  BDRate.h
//  BDTransactionViewer
//
//  Created by Tiago Bencardino on 27/08/16.
//  Copyright © 2016 Badoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BDRate : NSObject

@property (nonatomic, strong) NSString *from;
@property (nonatomic, strong) NSString *rate;
@property (nonatomic, strong) NSString *to;

- (id)initWithDictionary:(NSDictionary*)dictionary;

@end
