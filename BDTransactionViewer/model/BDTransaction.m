//
//  BDTransaction.m
//  BDTransactionViewer
//
//  Created by Tiago Bencardino on 27/08/16.
//  Copyright © 2016 Badoo. All rights reserved.
//

#import "BDTransaction.h"
#import "BDTransactionHelper.h"

NSString * const BDTransactionAmount = @"amount";
NSString * const BDTransactionCurrency = @"currency";
NSString * const BDTransactionSku = @"sku";

@implementation BDTransaction

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if (self = [super init]) {
        self.amount = dictionary[BDTransactionAmount];
        self.currency = dictionary[BDTransactionCurrency];
        self.sku = dictionary[BDTransactionSku];
    }
    
    return self;
}

- (NSString*)description {
    
    NSMutableString *string = [NSMutableString new];
    NSString *format = @"%@ : %@\n";
    
    [string appendString:[NSString stringWithFormat:format, BDTransactionAmount, self.amount]];
    [string appendString:[NSString stringWithFormat:format, BDTransactionCurrency, self.currency]];
    [string appendString:[NSString stringWithFormat:format, BDTransactionSku, self.sku]];
    
    return string;
}


//TODO: reimplement with NSFormatter + Locale given ISO 4217
- (NSString*)localizedAmout {
    
    if ([self.currency isEqualToString:@"CAD"]) {
        return [NSString stringWithFormat:@"CA$ %.2f", self.amount.floatValue];
    } else if ([self.currency isEqualToString:@"GBP"]) {
        return [NSString stringWithFormat:@"£ %.2f", self.amount.floatValue];
    } else if ([self.currency isEqualToString:@"AUD"]) {
        return [NSString stringWithFormat:@"AU$ %.2f", self.amount.floatValue];
    } else if ([self.currency isEqualToString:@"USD"]) {
        return [NSString stringWithFormat:@"$ %.2f", self.amount.floatValue];
    }
    
    return [NSString stringWithFormat:@"%.2f",self.amount.floatValue];
}

- (NSDecimalNumber*)GBPAmount {
    
   return [self.currency isEqualToString:@"GBP"] ? [NSDecimalNumber decimalNumberWithString:self.amount] : [[BDTransactionHelper sharedHelper] convertTransactionToGBP:self];
}

- (NSString*)localizedGBPAmount {
    
    return [NSString stringWithFormat:@"£ %.2f", [self GBPAmount].floatValue];
}

@end
