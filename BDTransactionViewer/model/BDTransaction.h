//
//  BDTransaction.h
//  BDTransactionViewer
//
//  Created by Tiago Bencardino on 27/08/16.
//  Copyright © 2016 Badoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BDTransaction : NSObject

@property (nonatomic, strong) NSString *amount;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *sku;

- (id)initWithDictionary:(NSDictionary*)dictionary;

- (NSString*)localizedGBPAmount;
- (NSString*)localizedAmout;
- (NSDecimalNumber*)GBPAmount;


@end
