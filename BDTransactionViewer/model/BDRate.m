//
//  BDRate.m
//  BDTransactionViewer
//
//  Created by Tiago Bencardino on 27/08/16.
//  Copyright © 2016 Badoo. All rights reserved.
//

#import "BDRate.h"

NSString * const BDRateFrom= @"from";
NSString * const BDRateRate = @"rate";
NSString * const BDRateTo = @"to";

@implementation BDRate

- (id)initWithDictionary:(NSDictionary *)dictionary {
    
    if (self = [super init]) {
        self.from = dictionary[BDRateFrom];
        self.rate = dictionary[BDRateRate];
        self.to = dictionary[BDRateTo];
    }
    
    return self;
}

- (NSString*)description {
    
    NSMutableString *string = [NSMutableString new];
    NSString *format = @"%@ : %@\n";
    
    [string appendString:[NSString stringWithFormat:format, BDRateFrom, self.from]];
    [string appendString:[NSString stringWithFormat:format, BDRateRate, self.rate]];
    [string appendString:[NSString stringWithFormat:format, BDRateTo, self.to]];
    
    return string;
}

@end
